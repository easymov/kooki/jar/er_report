@bibliography.book('ref_book', title='Book', author='Norbert', publisher='Leon', year='2018')
@bibliography.manual('ref_manual', title='Manual', author='Albert', organization='Dumas', year='2018')
@bibliography.misc('ref_website', title='Misc', author='Open Robotics', note='http://www.ros.org/is-ros-for-me/', year='2018')
@bibliography.phdthesis('ref_thesis', title='Php Thesis', author='Lambert', school='ISIMA', year='2018')