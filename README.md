# er_report

This kooki jar is an internship report template.

## Prerequisites

Make sure the Easymov document toolchain is installed and configured.
You can look at the tutorial at [gitlab.com/easymov/kooki/document_toolchain](https://gitlab.com/easymov/kooki/document_toolchain).

- kooki-xelatex

```
pip3 install kooki-xelatex
```

## Quickstart

```bash
kooki install er_report
gener8 er_report
# choose in the prompt the name of your folder
cd <folder_name>
kooki bake --update
```

Edit the *extensions* and the *metadata.yaml* to match your needs.

## Extensions

*abstract*
: The abstract

*annexes*
: The appendix

*conclusion*
: The conclusion

*content*
: The different parts

*introduction*
: The introduction

*remerciements*
: The acknowledgements

*introduction*
: The introduction

*resume*
: The abstract in french

## Metadata

```yaml
trainee: XXXXXXX XXXXXXX
supervisor: XXXXXXX XXXXXXX
tutor: XXXXXXX XXXXXXX
year: XXXX-XXXX
duration: XXXXX
title: XXXXXXXXX XXXXXXXXXX
subtitle: XXXXXXXXX XXXXXXXXXX
options: |
  XXXXXXXXXXXX XXXXXXXXXXX \\
  XXXXXXXXX XXXXXXXXXXXX

company:
  name: XXXXXX
  address: |
    xxxxxxxx xxxx xxxxxxxxx \\
    xxxxxxxx xxxx xxxxxxxxx
  logo:

school:
  name: XXXXXX
  address: |
    xxxxxxxx xxxx xxxxxxxxx \\
    xxxxxxxx xxxx xxxxxxxxx
  logo:
```

## Glossary

This template contains extensions to handle glossary entries.
You can add acronym and term definition to the glossary.
To do so use the following lines in your report.

```
@acronym('ROS', 'Robot Operating System')
@glossary('Glossary', 'a list of terms in a special subject, field, or area of usage, with accompanying definitions.')
```

In some specific cases you might want to add an acronym and also a specific definion.
To do so you can provide the definition as a third parameter of `@acronym`.

```
@acronym('ROS', 'Robot Operating System', 'ROS is a great platform.')
```

Thoses lines will not directly add thoses terms to your glossary.
You need to use them in your documents by using a specific command called `gls`.

For example, the following lines.

```
During my internship I had to learn how to use @gls('ROS').
```

## Bibliography

This template contains extensions to handle the bibliography entries.

First, you need to create a file called *bibliographie.md*.
Inside this file you can call the following extensions to add entry to your bibliography.

```
@bibliography.book('ref_book', title='Book', author='Norbert', publisher='Leon', year='2018')
@bibliography.manual('ref_manual', title='Manual', author='Albert', organization='Dumas', year='2018')
@bibliography.misc('ref_website', title='Misc', author='Open Robotics', note='http://www.ros.org/is-ros-for-me/', year='2018')
@bibliography.phdthesis('ref_thesis', title='Php Thesis', author='Lambert', school='ISIMA', year='2018')
```

**Do not put anything else than the calls to those extensions inside the *bibliographie.md* file**.
**If you do the project will probably not work.**

If you do not cite an entry it will not appear in the bibliography.
To cite something you need to use the following extension:

```
ROS has a permissive Licensing system @cite('ref_website').
```